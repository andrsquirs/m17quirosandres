﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PJMovement : MonoBehaviour
{

	/*
	SPEED es la variable que usaremos para mover a nuestro coche, variará dependiendo de si aceleramos, frenamos, usamos turbo... 
	*/
	private float SPEED = 0f;
	/*
	MAX_SPEED nos indica la fueza máxima que podremos ejercer, eso hace que se limite la velocidad una vez llega a cierto punto.
	*/
	private float MAX_SPEED = 270000f;
	/*
	MAX_ROTATE_SPEED limitara lo mucho que puede girar nuestro personaje.
	*/
	private float MAX_ROTATE_SPEED = 200f;
	/*
	ROTATE_SPEED es lo que podemos girar ahora mismo, está limitado por el MAX_ROTATE_SPEED y varía dependiendo de la SPEED que tengamos en el momento que giremos.
	*/
	private float ROTATE_SPEED = 200f;
	/*
	speedlimit es la acelaración, lo que hace es que su valor se suma al de SPEED, pero se ve limitado en base a la SPEED que tengamos, cuanto más rápido menos acelera.
	*/
	private float speedlimit = 2800f;
	/*
	JUMP es la variable de salto, es la fuerza que se ejercerá verticalemte a nuestro vehículo.
	*/
	private float JUMP = 0f;
	/*
	saltocd es la variable que nos mide cuanto tiempo ha pasado desde que saltamos, se ve limitada por maxsaltocd que si es igual a saltocd significa que podemos saltar.
	*/
	private float saltocd = 0.8f;
	private float maxsaltocd = 0.8f;
	/*
	salto nos indica si es true que podemos saltar, si es false no.
	*/
	private bool salto = true;
	/*
	grounded nos indica si nuestro vehículo está en el suelo.
	*/
	public static bool grounded=true;
	/*
	vuelta es el texto que aparece por pantalla con la vuelta actual en la que estamos.
	*/
	public UnityEngine.UI.Text vuelta;
	/*
	mvuelta muestra por pantalla el número y tiempo de la mejor vuelta que hemos hecho
	*/
	public UnityEngine.UI.Text mvuelta;
	/*
	actual muestra por pantalla el tiempo de la vuelta que estamos haciendo
	*/
	public UnityEngine.UI.Text actual;
	/*
	turbo muestra por pantalla el número de turbos que podemos usar.
	*/
	public UnityEngine.UI.Text turbo;
	/*
	nturbo es la variable interna de los turbos que podemos usar.
	*/
	private int nturbo = 0;
	/*
	nvuelta es la variable interna de la vuelta en la que estamos
	*/
	public int nvuelta = 1;
	/*
	mvueltan es el valor en segundos de la mejor vuelta que hemos hecho
	*/
	public float mvueltan =0f;
	/*
	mvueltanum es el valor interno de la vuelta donde se ha hecho el mejor tiempo
	*/
	public int mvueltanum = 0;
	/*
	vactual es el valor interno de la vuelta que está haciendo actualmente en segundos
	*/
	public float vactual;
	/*
	enturbo sirve para saber si está utilizando el turbo para no usarlo otra vez.
	*/
	private bool enturbo=false;
	/*
	cp1, cp2, cp3 y cp4 son los 4 checkpoints por los que tiene que pasar el vehículo para que le contabilice la vuelta.
	*/
	public bool cp1 = false;
	public bool cp2 = false;
	public bool cp3 = false;
	public bool cp4 = false;
	/*
	empezar me sirve para saber si es la primera vuelta cuando inicia el juego ya que el cp4 estará en false pero será porque ha empezado, no porque se lo haya saltado.
	*/
	private bool empezar = false;

	 private void Start() {
		 //Nada más empezar le pongo más gravedad al coche para que vaya más como me gusta.
		Physics.gravity=new Vector3(0,Physics.gravity.y*3.2f,0);
	}
	public void FixedUpdate ()
	{
		
		ProcessInput();
		ComprobarSalto();
		Debug.Log(SPEED);
		UpdateUI();
		

	}

	private void ProcessInput ()
	{
		//Todo el rato estará poniendo la fuerza vertical en base a la que tenga en JUMP, si estaba saltando bajará hasta 0
		this.GetComponent<Rigidbody>().AddForce(this.transform.up * JUMP * Time.deltaTime);
		if (grounded){
		//Para ir a la izquierda tiene que estar en movimiento, no quiero que rote el coche estando quieto
		if (Input.GetKey("left") || Input.GetKey("a")&&SPEED!=0)
		{
			//Dependiendo de si la fuerza es positiva o no, irá hacia una dirección y rotará de una forma.
			if(SPEED>0){
				ROTATE_SPEED=MAX_ROTATE_SPEED-(SPEED/2200);
			}
			else {
				ROTATE_SPEED=MAX_ROTATE_SPEED+(SPEED/2200);
			}
            /*
             * RotateAround, gira en funció d'un punt (fent servir un punt de referencia, un vector de rotació, i un angle)
             * El punt de referencia seria el centre de l'objecte. si vols rotar sobre tu mateix seria la propia position (recordem que la position implica les coordenades centrals)
             * el vector pots fer servir un dels tres vectors base
             *  transform.up -> gira al voltant de l'eix Y (fletxa verda)
             *  transform.forward -> gira al voltant de l'eix Z (fletxa blava)
             *  transform.right -> gira al voltant de l'eix X (fletxa vermella)
             * 
             * 
             * */


            /*
             * Time.deltaTime torna el temps entre Frames. D'aquesta forma t'assegures que la velocitat sigui independent del framerate. (un framerate mes baix fara que es mogui mes per frame)
             **/
			transform.RotateAround(transform.position, transform.up, Time.deltaTime * -ROTATE_SPEED);
		}
		//Lo mismo que lo de la izquierda pero con la derecha
		if (Input.GetKey("right") || Input.GetKey("d")&&SPEED!=0)
		{
			if(SPEED>0){
				ROTATE_SPEED=MAX_ROTATE_SPEED-(SPEED/2200);
			}
			else {
				ROTATE_SPEED=MAX_ROTATE_SPEED+(SPEED/2200);
			}
			transform.RotateAround(transform.position, transform.up, Time.deltaTime * ROTATE_SPEED);
		}
		//Aceleramos aumentando nuestra velocidad, pero tiene como limitante MAX_SPEED y speedlimit es nuestra aceleración que se reduce en base a la SPEED que ya tengamos antes
		if (Input.GetKey("up") || Input.GetKey("w")) { 
			
				SPEED+=(speedlimit-(SPEED/500));
			
				if (SPEED>MAX_SPEED){
					SPEED=MAX_SPEED;
				}
			
			//Empujamos hacia delante a nuestro vehículo
			this.GetComponent<Rigidbody>().AddForce(this.transform.forward * SPEED * Time.deltaTime);

		}
		//Freno y marcha atrás, va reduciendo la velocidad y también puede tener velocidad negativa para ir marcha atrás
		if (Input.GetKey("down") || Input.GetKey("s")) {
			if (SPEED>=(-150000f)) {
				SPEED-=5000;	
			}
			
			
			
			this.GetComponent<Rigidbody>().AddForce(this.transform.forward * SPEED * Time.deltaTime); 
			}
		//Al saltar le daremos una fuerza a JUMP, pondremos salto a false para no poder saltar otra vez y saltocd a 0 para que tenga un cooldown para usarlo de nuevo
		if (Input.GetKey("space")&&salto&&grounded){
			JUMP=200000;
			salto = false;
			saltocd = 0f;
		}
		//Mientras la fuerza de salto sea mayor a 0, se irá reduciendo
		else if (JUMP>0){
			JUMP-=12000;
		}
		//Al usar un turbo se reducirá en uno el número de turbos que tenemos (lógico) y enturbo será true.
		if (Input.GetKey("e")&&nturbo>0&&!enturbo){
			nturbo--;
			enturbo=true;
			//Mientras enturbo sea true, llamaremos a una couroutine durante 2 segundos que dará la velocidad de turbo.
			if (enturbo){
				
				StartCoroutine(RunTurbo(2.0f));
			}
			
		}
		//Mientras no apretemos a ninguna tecla de acelerar o frenar, la fuerza ya sea positiva o negativa irá volviendo poco a poco a 0 para quedarse quieto en el sitio
		if (!Input.GetKey("down")&&!Input.GetKey("s")&&!Input.GetKey("up")&&!Input.GetKey("w")){
			if (SPEED<0){
				SPEED+=3000;
		
				if (SPEED>=0){
					SPEED=0;
				}
				
			}
			else {
				SPEED-=2000;
		
				if (SPEED<=0){
					SPEED=0;
				}
			
			}
		
		}

		}
		
	}
	//Si salto cd supera a maxsaltocd, salto será true, lo podremos volver a usar.
	private void ComprobarSalto() {
		if (saltocd > maxsaltocd)
        {
            saltocd =maxsaltocd;
            salto=true;
        }
        //Si no, seguirá sumando a saltocd
        else
        {
            saltocd += Time.deltaTime;
        }
	}

		//PRUEBAS
		void OnCollisionEnter(Collision collision)
    	{
	

     	 if (collision.gameObject.tag=="Circuito"){
		 	 grounded=true;
		
	  	}
	  	else {
		 	 grounded=true;
			
	  	}

      
		
    }
	//Aquí actualizamos toda la interfaz
	private void UpdateUI(){
		//Mostramos por pantalla el número de la vuelta actual
		vuelta.text="vuelta : "+nvuelta;
		//El valor interno y su actualización en interfaz de la vuelta que estamos corriendo actualmente
		vactual+=Time.deltaTime;
		actual.text="actual: "+vactual.ToString("f3");
		//Mostramos por pantalla el número y tiempo de la mejor vuelta
		mvuelta.text="mejor vuelta: "+mvueltanum+"\n"+mvueltan.ToString("f3");
		//Actualizamos por pantalla el número de turbos que tenemos disponibles
		turbo.text="x"+nturbo;
	}
	//Aquí es donde miramos si ha pasado por los checkpoints en orden.
	 void OnTriggerEnter(Collider other) {
		 //Si ha pasado por el primero y pasó por el último
		if (other.gameObject.name.Equals("CP1")&&cp4){
			//Lo que hacemos es poner false que ha pasado por el Checkpoint 4 y true que ha pasado por el primero para luego cuando pase por los siguientes pueda ver si por el último que ha pasado es el correcto
			cp1=true;
			cp4=false;
			//SI no hay vuelta rápida, pondremos la que tengamos actualmente al pasar por meta
			if (mvueltan==0){
				mvueltan=vactual;
				mvueltanum=nvuelta;
			}
			//Si no, solo la pondrá si el tiempo es menor a la que sea más rápida.
			else if(vactual<mvueltan){
				mvueltan=vactual;
				mvueltanum=nvuelta;
			}
			//Aumentamos el número de vuelta actual y el número de turbo y ponemos a 0 el tiempo actual de vuelta
			nvuelta++;
			vactual=0f;
			nturbo++;
		}
		//Esto es en caso de iniciar el juego, que no hemos pasado por el último checkpoint, ponemos un booleano para ver que hemos empezado.
		else if (other.gameObject.name.Equals("CP1")&&!empezar) {
			//Ponemos el turbo a 1, ponemos empezar y el cp1 a true y el tiempo de vuelta a 0 y el número de vuelta a 1.
			nturbo++;
			empezar=true;
			cp1=true;
			nvuelta=1;
			vactual=0f;
		}
		//Aquí comprobamos si al pasar por el checkpoint si han pasado por el checkpoint anterior a ellos
		if (other.gameObject.name.Equals("CP2")&&cp1){
			cp2=true;
			cp1=false;
		}
		if (other.gameObject.name.Equals("CP3")&&cp2){
			cp3=true;
			cp2=false;
		}
		if (other.gameObject.name.Equals("CP4")&&cp3){
			cp4=true;
			cp3=false;
		}
	}
	//Cuando gastamos un turbo durante un tiempo la velocidad actual y máxima aumentará, al pasar ese tiempo volverá al la velocidad máxima posible de base.
	IEnumerator RunTurbo(float tiempo){
        MAX_SPEED=400000;
		SPEED=400000;
		MAX_ROTATE_SPEED = 300f;
        
       yield return new WaitForSecondsRealtime(tiempo);
	   MAX_ROTATE_SPEED = 200f;
		MAX_SPEED=270000;
		SPEED=270000;
       enturbo=false;
    }
	
	


}
