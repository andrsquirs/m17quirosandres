﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
    public Camera mainCamera;
    public PJMovement player;
//    public GameObject forw = new GameObject();
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        // LERP fa una interpolació lineal. Aixo fara un moviment de camara molt més smooth que no si directament ho posem a la posició del jugador
		mainCamera.transform.position = new Vector3
		(
            //volem la posició no exactament en el jugador, sino que la volem una mica enrere i damunt seu. Si el jugador gira la camara s'hauria de moure respecte al seu nou darrere, orbitant
			Mathf.Lerp(mainCamera.transform.position.x, (player.transform.position.x - (player.transform.forward.x * 10)), Time.deltaTime * 15),
			Mathf.Lerp(mainCamera.transform.position.y, (player.transform.position.y + (player.transform.forward.y * 5))+3, Time.deltaTime * 15),
			Mathf.Lerp(mainCamera.transform.position.z, (player.transform.position.z - (player.transform.forward.z * 10)), Time.deltaTime * 15)
		);

        //fa una rotació automàtica per a que miri al jugador
		mainCamera.transform.LookAt(player.transform);

      

        Vector3 pos = new Vector3(player.transform.position.x + (player.transform.forward.x), player.transform.position.y, player.transform.position.z + (player.transform.forward.z ));
        //forw.transform.position = pos;
    }
}
