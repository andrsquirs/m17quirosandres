﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraSigue : MonoBehaviour
{
    //Player hace referencia al propio personaje del jugador
    public GameObject Player;
    //gameCamera hace referencia a la cámara principal del juego que sigue al pesonaje
    public Camera gameCamera;
    //La array de prefabs de terrenos que se van generando automáticamente
    public GameObject[] blockPrefabs;
    //El puntero es un número que sirve para medir donde crear el nuevo terreno y si la X del personaje la supera generaría el terreno
    private float puntero;
    //El número de distancia que se suma al puntero para generar un nuevo terreno
    private float spawn = 100;
    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        //Únicamente tranformo la posición X de la cámara con la del jugador para que no se mueva verticalmente cuando el jugador cambie la gravedad
        gameCamera.transform.position = new Vector3(
            Player.transform.position.x,
            gameCamera.transform.position.y,
            gameCamera.transform.position.z);
        //Si la posición X del jugador supera al número del puntero se genera un terreno
           if (Player != null && this.puntero < Player.transform.position.x + spawn )
        {
            //Hace aparecer un terreno aleatorio de la array menos el primero
            GameObject newBlock = Instantiate(blockPrefabs[Random.Range(1, blockPrefabs.Length)]);
            Debug.Log(Random.Range(1, blockPrefabs.Length));
            //cogemos el tamaño, que seria la escala (tamaño) de x, del primer hijo (el suelo) del bloque prefab
            float size = newBlock.transform.GetChild(0).transform.localScale.x;
            //ponemos la posicion del tamaño instanciado en la posicion del puntero + la mitad de su tamaño (la posicion que tenemos que indicarle es su centro absoluto)
            newBlock.transform.position = new Vector2(puntero+size/2, 0);
            //aumentamos el puntero en el tamaño del bloque
            puntero += size;
        }
    }
  
}