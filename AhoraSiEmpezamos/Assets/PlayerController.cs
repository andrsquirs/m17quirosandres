﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //Es un tiempo de espera para que al pulsar a la gravedad el juego no lo entienda como varias pulsaciones consecutivas
    public int gcd = 10;
    //Número de puntos en formato integer
    public int count = 0;
    //Esta string sirve para transformar los puntos a texto para luego ponerlo en el texto de la interfaz
    public String countText;
    //Este es el texto que aparece por pantalla
    public UnityEngine.UI.Text score;
   // Controles en pantalla
    public UnityEngine.UI.Text controles;
    //Booleano para quitar los controles en pantalla
    public Boolean tecla = true;
     // Start is called before the first frame update
    void Start()
    {
        //Empezamos los puntos a 0, lo transformamos a string y lo metemos en el texto que aparece por pantalla en el juego
        count = 0;
        countText = (""+count);
        score.text = "Puntos: "+countText;  
        
    }
    void OnCollisionEnter2D(Collision2D col)
    {
         //Si tocas un triangulo te mueres
        if (col.gameObject.name == "Triangle")
        {
            Destroy(this.gameObject);
        }
       
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        //Si tocas una moneda la cual se llama punto en este caso, suma 1 al contador de puntos y destruye la moneda
         if (col.gameObject.name == "Punto"){
            Debug.Log("Ha pasa'o");
            count++;
            Destroy(col.gameObject);
            
            
        }
    
       
    }
   

    // Update is called once per frame
    void Update()
    {
        //Constantemente está cambiando los puntos a texto y poniendolos en la pantalla de juego
        countText = (""+count);
        score.text=("Puntos: "+countText);
        if (tecla){
            controles.text= "Pulsa 'V' para cambiar gravedad";
        }
        else {
            controles.text="";
        }
        //Si pulsa la tecla V cambia la gravedad todo el rato y también le da la vuelta al sprite por el eje de la Y, además pone el temporidador a 0 para que no se pulse varias veces
        if (Input.GetKey("v")&&gcd==10)
        {
            tecla=false;
            this.GetComponent<Rigidbody2D>().gravityScale *= -1;
            if(this.GetComponent<SpriteRenderer>().flipY == false){
                this.GetComponent<SpriteRenderer>().flipY = true;
            } else {
                this.GetComponent<SpriteRenderer>().flipY = false;
            }
            gcd = 0;
        }
        //Va sumando al temporizador para poder usar de nuevo la tecla V
        if (gcd < 10)
        {
            gcd++;
        }
        //La velocidad del personaje
        this.GetComponent<Rigidbody2D>().velocity=(new Vector2(13, GetComponent<Rigidbody2D>().velocity.y));
    }
   
}
