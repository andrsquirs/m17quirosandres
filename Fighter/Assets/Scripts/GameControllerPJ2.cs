﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameControllerPJ2 : MonoBehaviour
{
    public int vel = 4;
    public char dir = 'd';
    public bool grounded = true;
    public bool caminandod = false;
    public bool punch = false;
    public bool agachado = false;
    public int hp1 = 100;
    public GameObject shurikend;
    public GameObject shurikeni;
    public int combo = 0;
    public GameObject opj = GameObject.Find("PJ1");
     public delegate void _OnDamaged (float vida);
    public event _OnDamaged OnDamaged;
    public bool inmo = false;
    public bool inv = false;
    public Image hbar;
    public bool volt = false;
    public bool volt2 = false;
    public bool hit= false;
    public bool block = false;
    // Start is called before the first frame update
    void Start()
    {
        opj = GameObject.Find("PJ1");
    }

    // Update is called once per frame
    void Update()
    {   
        //this.transform.GetChild(0).gameObject.SetActive(false);
         this.GetComponent<Animator>().SetBool("Voltereta",volt);
         this.GetComponent<Animator>().SetInteger("Combo",combo);
         this.GetComponent<Animator>().SetBool("Hit",hit);
          this.GetComponent<Animator>().SetBool("Punch",punch);
          this.GetComponent<Animator>().SetBool("Block",block);
        if (!inmo&&inv==false){
             this.GetComponent<Animator>().SetBool("Grounded",grounded);
            this.GetComponent<Animator>().SetBool("Caminandod",caminandod);
        this.GetComponent<Animator>().SetBool("Agachado",agachado);
       
        
       
        if (Input.GetKey("i")&&grounded){
            this.GetComponent<Rigidbody2D>().velocity = (new Vector2(GetComponent<Rigidbody2D>().velocity.x,9));
            grounded=false;
            agachado=false;
        }
        if (Input.GetKeyUp("l")&&!volt2){
            StartCoroutine(VolteretaD(0.3f));
     
        }
        if (Input.GetKeyDown("l")&&volt2&&grounded){
             this.GetComponent<Rigidbody2D>().AddForce(new Vector2(200,0));
             StartCoroutine(VolteretaDA(0.5f));
             StartCoroutine(Inmov(0.5f));
        }
        if (Input.GetKeyUp("j")&&!volt2){
            StartCoroutine(VolteretaD(0.3f));
     
        }
        if (Input.GetKeyDown("j")&&volt2&&grounded){
             this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-200,0));
             StartCoroutine(VolteretaDA(0.5f));
             StartCoroutine(Inmov(0.5f));
        }
        if (Input.GetKey("l")){
            dir = 'd';
            this.GetComponent<Rigidbody2D>().velocity = (new Vector2(vel,GetComponent<Rigidbody2D>().velocity.y));
            caminandod=true;
             this.transform.localScale=new Vector2(3,3);
             agachado=false;
             
            
        }
         else if (Input.GetKey("j")){
            dir = 'i';
            this.GetComponent<Rigidbody2D>().velocity = (new Vector2(-vel,GetComponent<Rigidbody2D>().velocity.y));
            caminandod=true;
            this.transform.localScale=new Vector2(-3,3);
            agachado=false;
        }
        else if (Input.GetKey("k")&&grounded){
                agachado=true;
        }
         else if (Input.GetKey("h")&&grounded){
                block=true;
        }
        else if (Input.GetKeyDown("o")&&grounded){
                StartCoroutine(ComboPunch(0.7f));
                //StartCoroutine(Inmov(0.5f));
        }
        else if (Input.GetKeyDown("u")){
            if (dir=='d'){
                Invoke("ShurikenD",0);
            }
             else if (dir=='i'){
                Invoke("ShurikenI",0);
            }
            
            StartCoroutine(Inmov(0.5f));
        }
          else if (Input.GetKey("m")&&grounded){
                block=true;
                
        }
        else {
            if(grounded){
                 this.GetComponent<Rigidbody2D>().velocity = (new Vector2(0,GetComponent<Rigidbody2D>().velocity.y));
            }
           
            if (dir=='d'){
                caminandod=false;
                agachado=false;
                //punch = false;
                block=false;
            }
            else  if (dir=='i'){
               caminandod=false;
               agachado=false;
                //punch = false;
                block=false;
            }
        }
        }
      
        
       
       
    }
    void OnCollisionEnter2D(Collision2D col)
    {

        if (col.gameObject.tag == "Suelo")
        {
            grounded=true;
            this.GetComponent<Animator>().SetBool("Grounded",true);
        }
        if (col.gameObject.tag == "Pincho"&&inv==false){
            StartCoroutine(Invul(1f));
            StartCoroutine(Hitt(1f));
           
            hp1 -=15;
            OnDamaged(15);
            if (dir=='d'){
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-200,200));
            }
            else if (dir=='i'){
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(200,200));
            }
            if (hp1<=0){
            SceneManager.LoadScene("F");
            }
        }
        if (col.collider.tag=="Punetaso"&&!inv&&!block){
            Debug.Log("HEY");
            if (opj.GetComponent<GameController>().combo==1){
                hp1 -=6;
                OnDamaged(6); 
            }
            else if (opj.GetComponent<GameController>().combo==2){
                hp1 -=10;
                OnDamaged(10); 
                StartCoroutine(Invul(1f));
                StartCoroutine(Hitt(1f));
                  if (dir=='d'){
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-300,300));
                    }
                 else if (dir=='i'){
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(300,300));
                 }
            }
        }
       
       
    }
    void OnTriggerEnter2D(Collider2D col)
    {
         if (col.gameObject.name=="ShurikenD"&&!inv&&!block){
             Destroy(col.gameObject);
              hp1 -=5;
                OnDamaged(5); 
                StartCoroutine(Invul(1f));
                StartCoroutine(Hitt(1f));
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(100,100));
        }
         if (col.gameObject.name=="ShurikenI"&&!inv&&!block){
             Destroy(col.gameObject);
              hp1 -=5;
                OnDamaged(5); 
                StartCoroutine(Invul(1f));
                StartCoroutine(Hitt(1f));
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-100,100));
        } 
        
    }
    void ShurikenD(){
        GameObject newShuriken = Instantiate(shurikend, this.transform);
        newShuriken.transform.parent=null;
    }
    void ShurikenI(){
        GameObject newShuriken = Instantiate(shurikeni, this.transform);
        newShuriken.transform.parent=null;
    }
    IEnumerator Invul(float tiempo){
       inv = true; 
       yield return new WaitForSecondsRealtime(tiempo);
       inv = false;
    }
    IEnumerator Hitt(float tiempo){
        hit= true; 
      
        
       yield return new WaitForSecondsRealtime(tiempo);
       hit=false;
    }
    IEnumerator ComboPunch(float tiempo){
        punch=true;
         combo++;
       if (combo==3){
           combo=0;
       }
        yield return new WaitForSecondsRealtime(tiempo);
        punch=false;
        combo=0;
    }
    IEnumerator Inmov(float tiempo){
        inmo= true; 
       yield return new WaitForSecondsRealtime(tiempo);
       inmo=false;
    }
    IEnumerator VolteretaD(float tiempo){
        volt2=true;
        yield return new WaitForSecondsRealtime(tiempo);
        volt2=false;
    }
     IEnumerator VolteretaDA(float tiempo){
        volt=true;
        yield return new WaitForSecondsRealtime(tiempo);
        volt=false;
    }
   
  
}