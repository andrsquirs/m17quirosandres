﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShurikenD : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.transform.position = new Vector2(GameObject.Find("PJ1").transform.position.x,GameObject.Find("PJ1").transform.position.y+1);
        this.GetComponent<Rigidbody2D>().velocity = (new Vector2(6,6));
        this.name="ShurikenD";
    }

    // Update is called once per frame
    void Update()
    {
        
        
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Suelo")
        {
           Destroy(this.gameObject);
        }  
         if (col.gameObject.name == "PJ2")
        {
            Destroy(this.gameObject);
        } 
    }
}
