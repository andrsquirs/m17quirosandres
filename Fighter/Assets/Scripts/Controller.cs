﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Controller : MonoBehaviour
{
    // Start is called before the first frame update
    public GameController pj1;
    public GameControllerPJ2 pj2;
    public Image hbar1;

    public Image hbar2;


    void Start()
    {
        pj1.OnDamaged +=pj1Damage;
         pj2.OnDamaged +=pj2Damage;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void pj1Damage(float dano){
        hbar1.fillAmount -=(dano/100);
    }
    void pj2Damage(float dano){
        hbar2.fillAmount -=(dano/100);
    }
}
