﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameController : MonoBehaviour
{
    //Velocidad
    public int vel = 2;
    //Char para saber al dirección
    public char dir = 'd';
    //Boolean para saber si está en el suelo
    public bool grounded = true;
    //Boolean apra saber si está caminando
    public bool caminandod = false;
    //Boolean para saber si está en combo
    public bool punch = false;
    //Boolean si está agachado
    public bool agachado = false;
    //La variable de vida en númerica
    public int hp1 = 100;
    //Los shuriken que se generan con la Q
    public GameObject shurikend;
    public GameObject shurikeni;
    //Contar el combo
    public int combo = 0;

    public GameObject opj = GameObject.Find("PJ2");

    //Boolean para no moverse o hacer otra acciones
    public bool inmo = false;
    //Boolan para no recibir daño
    public bool inv = false;
    //La imagen de la vida para que baje
    public Image hbar;
    //Booleans para hacer la voltereta
    public bool volt = false;
    public bool volt2 = false;
    //Boolean de recibir gole
    public bool hit= false;
    //Boolean de bloqueo
    public bool block=false;
    public delegate void _OnDamaged (float vida);
    public event _OnDamaged OnDamaged;
    // Start is called before the first frame update
    void Start()
    {
        opj = GameObject.Find("PJ2");
    }

    // Update is called once per frame
    void Update()
    {   //Compruebo todos los boolean para las animaciones
        
         this.GetComponent<Animator>().SetBool("Voltereta",volt);
         this.GetComponent<Animator>().SetInteger("Combo",combo);
         this.GetComponent<Animator>().SetBool("Hit",hit);
          this.GetComponent<Animator>().SetBool("Punch",punch);
          this.GetComponent<Animator>().SetBool("Block",block);
        if (!inmo&&inv==false){
             this.GetComponent<Animator>().SetBool("Grounded",grounded);
            this.GetComponent<Animator>().SetBool("Caminandod",caminandod);
        this.GetComponent<Animator>().SetBool("Agachado",agachado);
       
        
       //Salto
        if (Input.GetKey("w")&&grounded){
            this.GetComponent<Rigidbody2D>().velocity = (new Vector2(GetComponent<Rigidbody2D>().velocity.x,7));
            grounded=false;
            agachado=false;
        }
        //Empiezo la voltereta
        if (Input.GetKeyUp("d")&&!volt2){
            StartCoroutine(VolteretaD(0.3f));
     
        }
        //La hago
        if (Input.GetKeyDown("d")&&volt2&&grounded){
             this.GetComponent<Rigidbody2D>().AddForce(new Vector2(200,0));
             StartCoroutine(VolteretaDA(0.5f));
             StartCoroutine(Inmov(0.5f));
        }
        //Lo mismo de la voltereta pero hacia la izquierda
        if (Input.GetKeyUp("a")&&!volt2){
            StartCoroutine(VolteretaD(0.3f));
     
        }
        if (Input.GetKeyDown("a")&&volt2&&grounded){
             this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-200,0));
             StartCoroutine(VolteretaDA(0.5f));
             StartCoroutine(Inmov(0.5f));
        }
        //Movimiento norma de izquierda a derecha
        if (Input.GetKey("d")){
            dir = 'd';
            this.GetComponent<Rigidbody2D>().velocity = (new Vector2(vel,GetComponent<Rigidbody2D>().velocity.y));
            caminandod=true;
             this.transform.localScale=new Vector2(3,3);
             agachado=false;
             
            
        }
         else if (Input.GetKey("a")){
            dir = 'i';
            this.GetComponent<Rigidbody2D>().velocity = (new Vector2(-vel,GetComponent<Rigidbody2D>().velocity.y));
            caminandod=true;
            this.transform.localScale=new Vector2(-3,3);
            agachado=false;
        }
        //Agachado
        else if (Input.GetKey("s")&&grounded){
                agachado=true;
        }
        //Combo de puñetazo
        else if (Input.GetKeyDown("e")&&grounded){
                StartCoroutine(ComboPunch(0.7f));
                //StartCoroutine(Inmov(0.5f));
        }
        //Bloqueando
        else if (Input.GetKey("x")&&grounded){
                block=true;
                
        }
       //Shurikens
        else if (Input.GetKeyDown("q")){
            if (dir=='d'){
                Invoke("ShurikenD",0);
            }
             else if (dir=='i'){
                Invoke("ShurikenI",0);
            }
            
            StartCoroutine(Inmov(0.5f));
        }
        //Estar quieto y hacer comprobaciones de algunos booleanos
        else {
            if(grounded){
                 this.GetComponent<Rigidbody2D>().velocity = (new Vector2(0,GetComponent<Rigidbody2D>().velocity.y));
            }
           
            if (dir=='d'){
                caminandod=false;
                agachado=false;
                //punch = false;
                block=false;
                
            }
            else  if (dir=='i'){
               caminandod=false;
               agachado=false;
                //punch = false;
                block=false;
               
            }
        }
        }
      
        
       
       
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        //Para ponerse en el suelo
        if (col.gameObject.tag == "Suelo")
        {
            grounded=true;
            this.GetComponent<Animator>().SetBool("Grounded",true);
        }
        //Si te golpeas con los pinchos del escenario recibes daño
        if (col.gameObject.tag == "Pincho"&&inv==false){
            StartCoroutine(Invul(1f));
            StartCoroutine(Hitt(1f));
            hp1 -=10;
            OnDamaged(10);
            if (dir=='d'){
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-200,200));
            }
            else if (dir=='i'){
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(200,200));
            }
            
            if (hp1<=0){
            SceneManager.LoadScene("F");
            }
        }
        //Si te pega con el puñetazo pero también mira en que parte del combo está
        if (col.collider.tag=="Punetaso2"&&!inv&&!block){
            Debug.Log("HEY");
            if (opj.GetComponent<GameControllerPJ2>().combo==1){
                hp1 -=5;
                OnDamaged(5); 
            }
            else if (opj.GetComponent<GameControllerPJ2>().combo==2){
                hp1 -=8;
                OnDamaged(8); 
                StartCoroutine(Invul(1f));
                StartCoroutine(Hitt(1f));
                  if (dir=='d'){
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-200,200));
                    }
                 else if (dir=='i'){
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(200,200));
                 }
            }
        }
      
       
     
       
    }
     void OnTriggerEnter2D(Collider2D col)
    {
          //Comprueba si colisiona con los shuriken
         if (col.gameObject.name=="ShurikenD2"&&!inv&&!block){
             Destroy(col.gameObject);
              hp1 -=5;
                OnDamaged(5); 
                StartCoroutine(Invul(1f));
                StartCoroutine(Hitt(1f));
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(100,100));
        }
         if (col.gameObject.name=="ShurikenI2"&&!inv&&!block){
             Destroy(col.gameObject);
              hp1 -=5;
                OnDamaged(5); 
                StartCoroutine(Invul(1f));
                StartCoroutine(Hitt(1f));
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-100,100));
        } 
        
    }
    //Shurikens
    void ShurikenD(){
        GameObject newShuriken = Instantiate(shurikend, this.transform);
        newShuriken.transform.parent=null;
    }
    void ShurikenI(){
        GameObject newShuriken = Instantiate(shurikeni, this.transform);
        newShuriken.transform.parent=null;
    }
    //Invulnerabildiad
    IEnumerator Invul(float tiempo){
       inv = true; 
       yield return new WaitForSecondsRealtime(tiempo);
       inv = false;
    }
    //Cuando te golpean
    IEnumerator Hitt(float tiempo){
        hit= true; 
      
        
       yield return new WaitForSecondsRealtime(tiempo);
       hit=false;
    }
    //Hacer el combo y contabilizar por el número de golpe por el que va
    IEnumerator ComboPunch(float tiempo){
        punch=true;
         combo++;
       if (combo==3){
           combo=0;
       }
        yield return new WaitForSecondsRealtime(tiempo);
        punch=false;
        combo=0;
    }
    //Inmovilizado
    IEnumerator Inmov(float tiempo){
        inmo= true; 
       yield return new WaitForSecondsRealtime(tiempo);
       inmo=false;
    }
    //Voltereta y comprobacione
    IEnumerator VolteretaD(float tiempo){
        volt2=true;
        yield return new WaitForSecondsRealtime(tiempo);
        volt2=false;
    }
     IEnumerator VolteretaDA(float tiempo){
        volt=true;
        yield return new WaitForSecondsRealtime(tiempo);
        volt=false;
    }
    
  
}
