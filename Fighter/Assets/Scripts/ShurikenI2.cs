﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShurikenI2 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.transform.position = new Vector2(GameObject.Find("PJ2").transform.position.x,GameObject.Find("PJ2").transform.position.y+1);
        this.GetComponent<Rigidbody2D>().velocity = (new Vector2(-6,6));
        this.name="ShurikenI2";
    }

    // Update is called once per frame
    void Update()
    {
        
        
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Suelo")
        {
           Destroy(this.gameObject);
        }  
         if (col.gameObject.name == "PJ1")
        {
           Destroy(this.gameObject);
        } 
    }
}
